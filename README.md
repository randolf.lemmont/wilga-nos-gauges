Wilga NOS Upgrade & FIP Gauges
==============================

[GotFriends PZL-104][wil] Wilga NOS upgrade and convenience FIP gauges.

Tested with the Wilga 80P variant, but should work with other variants,
possibly with minor tweaks.

[wil]: https://www.got-friends.com/products/pzl-104-wilga-35-80-series

Links
-----

* [Project repository][git]
* [Download ZIP archive][zip]

[git]: https://gitlab.com/randolf.lemmont/wilga-nos-gauges
[zip]: https://gitlab.com/randolf.lemmont/wilga-nos-gauges/-/archive/master/wilga-nos-gauges-master.zip


FIP Gauges
----------

There are two instruments provided. _WilgaMonitor_ is a readout of engine
stress paramters and _WilgaStatus_ that shows the following parameters:

* rudder & aileron trim
* engine primer state
* remaining NOS
* DME display
* parking brake

![Wilga FIP gauge screenshot](WilgaFIPGauge.jpg "Wilga FIP gauge screenshot")

The Gauges are based on Lorby's Status Panel and are made to work with [Lorby's
Axis And Ohs][aao] software. Compatibility with other software is possible, but
not tested.

[aao]: https://www.axisandohs.com/

To install, place files from this repository (archive) into the directory where
your software expects FIP gauge files, for example
`Documents\LorbyAxisAndOhs Files\UserGauges\WilgaStatus` and install the
provided _Quartz_ font file `Quartz-Medium.otf` found in this package.


NOS Upgrade
-----------

The upgrade is provided as a kit that has to be installed by the user (or
contracted mechanic) by editing files in your Wilga addon.

Warning: as with any aftermarket modification, you risk serious damage to your
engine. Use with caution and at your own risk.

Locate the file `gotfriends-wilga\SimObjects\Airplanes\GotFriends_Wilga_80P\engines.cfg`
and make a backup copy of it.

In this file then find the section begining with line `[PISTON_ENGINE]` and
place the following lines into that section, for example right below the
`[PISTON_ENGINE]` line:

```
emergency_boost_type = 2
max_emergency_boost_time = 275
emergency_boost_mp_offset = 8
emergency_boost_mp_damage = 36.0
emergency_boost_gain_offset = 0.70
emergency_boost_can_be_stopped = 1
emergency_boost_throttle_threshold = 0.95
```

The NOS is turned on and off using the WAR_EMERGENCY_POWER event with values 1 and 0.


Decouple Flight Director from Heading Bug
-----------------------------------------

With this modification you can move the HSI heading bug without setting flight
director bank angle.

Note: this will also prevent you from adjusting the flight director using
standard heading select events. You will have to use custom L-var `(Z:FdBank,
Number)` (in degrees between -30 and 30) for controlling flight director bank
angle.

In file `gotfriends-wilga\SimObjects\Airplanes\GotFriends_Wilga_80P\model\wilga_80P_int.xml`
make a backup copy of the file, then open it in editor, locate the following lines

```
(* Use Autopilot Heading Hold data to adjust Flight Director bank thru keybinds *)
(A:AUTOPILOT HEADING LOCK DIR, Degrees) (&gt;O:FdApBank)
```

and delete the second line, the one starting with `(A:AUTOPILOT...`.
